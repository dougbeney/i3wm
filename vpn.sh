#!/bin/bash

network_device=`nmcli device status | grep connected | grep -i connection | grep -o --regexp="^\w\+"`
vpn_status=`nmcli con | grep -i vpn | grep -o $network_device`

if [ "$vpn_status" = "eno1" ]; then
    echo -e "VPN Enabled"
fi
